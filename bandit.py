import numpy as np
from scipy.stats import bernoulli


class GaussianBandit(object):
    def __init__(self, mu, sigma=1):
        self.mu = mu
        self.max_gap = np.max(mu)
        self.gaps = self.max_gap - mu
        self.sigma = sigma
        self.K = mu.shape[0]
        self.cov = sigma * np.ones(self.K)

    def pull(self, i):
        return np.random.normal(loc=self.mu[i], scale=self.sigma)

    def sample(self, N):
        return np.random.normal(loc=self.mu, scale=self.cov, size=(N, self.K))


class BernoulliBandit(object):
    def __init__(self, mu, sigma=None):
        self.mu = mu
        self.max_gap = np.max(mu)
        self.gaps = self.max_gap - mu
        self.K = mu.shape[0]

    def pull(self, a):
        pass

    def sample(self, N):
        samples = bernoulli.rvs(self.mu, size=(N, self.K))
        return samples


class UniformBandit(object):
    def __init__(self, mu):
        self.mu = mu
        self.max_gap = np.max(mu)
        self.gaps = self.max_gap - mu
        self.K = mu.shape[0]

    def pull(self, a):
        pass

    def sample(self, N):
        samples = 0.5 * np.random.rand(N, self.K) + self.mu
        return samples
