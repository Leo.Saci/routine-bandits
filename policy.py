import numpy as np
from numpy.linalg import eigh, norm, pinv


class Policy(object):
    def __init__(self):
        pass

    def __str__(self):
        """
        Return the policy name
        (type str)
        """
        pass

    def __color__(self):
        """
        Return the color associated to the policy
        (type str)
        """

    def choose(self, agent):
        """
        Choose an action and return it 
        (type int)
        """
        pass


class Random(Policy):
    def __init__(self):
        pass

    def __str__(self):
        return "Random"

    def __color__(self):
        return "grey"

    def choose(self, agent):
        if agent.t < K:
            a_t = int(t)
        else:
            a_t = int(np.random.choice([i for i in range(K)]))
        return a_t


class KLUCB(Policy):
    def __init__(self):
        self.marker = "s"

    def __str__(self):
        return "KLUCB"

    def __color__(self):
        return "g"

    def choose(self, agent):
        t = agent.t
        T = agent.T
        K = agent.K
        h = agent.h

        if t < 3 * K:
            a_t = int(t % K)
        else:
            emp_means = agent.emp_mean[h]
            deviations = agent.deviation[h]
            a_t = int(np.argmax(emp_means + deviations))
        return a_t


class KLUCB_RB(Policy):
    def __init__(self):
        self.marker = "*"

    def __str__(self):
        return "KLUCB-RB"

    def __color__(self):
        return "r"

    def choose(self, agent):
        h = agent.h
        t = agent.t
        T = agent.T
        K = agent.K

        if t < K:
            a_t = int(t % K)
        elif h == 0:
            a_t = np.argmax(agent.emp_mean[h] + agent.deviation[h])
        else:

            idx = list(range(K))
            np.random.shuffle(idx)

            # Random arm index among the most pulled ones
            i_max = idx[np.argmax(agent.N[h, idx])]

            # test
            positives = self.test(agent, h, i_max)
            nb_pos = len(positives)

            # agregated data
            agg_N = np.sum(agent.N[positives], axis=0) + agent.N[h]
            agg_sums = np.sum(agent.sums[positives], axis=0) + agent.sums[h]
            agg_means = agg_sums / agg_N
            y = ((nb_pos + 1) * T) / (K * agg_N)
            f_h_t = agent.log_plus(y * (agent.log_plus(y) ** 2 + 1))
            agg_deviation = np.sqrt((2 * f_h_t) / agg_N)

            y = (t + 1) / (K * agent.N[h])
            f_t = agent.log_plus(y * (agent.log_plus(y) ** 2 + 1))
            dev = np.sqrt((2 * f_t) / agent.N[h])

            index = np.zeros((2, K))
            index[0] = agg_means + agg_deviation
            index[1] = agent.emp_mean[h] + dev
            index = np.min(index, axis=0)
            a_t = int(np.argmax(index))
        return a_t

    def test(self, agent, h, i_max):
        """
        Return the list of periods k <= h-1 positively tested
        (type list)
        """
        Z = 100 * np.abs(agent.most_pulled[:h] - i_max) * np.ones((agent.K, h))
        Z = Z.T
        Z_0 = (
            np.abs(agent.emp_mean[:h] - agent.emp_mean[h])
            - agent.deviation[:h]
            - agent.deviation[h]
        )
        Z = Z + Z_0
        Z = np.max(Z, axis=1)
        Z_0 = np.max(Z_0, axis=1)
        positives = set(np.where(Z <= 0)[0])
        return list(positives)


class tUCB(Policy):
    def __init__(self, M, K):
        self.marker = "^"
        self.M = M
        self.deviation_h = np.zeros(K)
        self.deviation = 0
        self.emp_mean_h = np.zeros(K)
        self.M2 = np.zeros((K, K))
        self.M3 = np.zeros((K, K, K))
        self.K = K

    def __str__(self):
        return "tUCB"

    def __color__(self):
        return "purple"

    def choose(self, agent):
        h = agent.h
        t = agent.t
        T = agent.T
        K = agent.K
        M = agent.M
        if t < 3 * K:
            a = int(t % K)
        elif h < M:
            n = agent.N[h]
            y = T / (K * n)
            f_t = agent.log_plus(y * (agent.log_plus(y) ** 2 + 1))
            dev = np.sqrt((2 * f_t) / n)
            a = np.argmax(self.emp_mean_h + dev)
        else:
            # test
            positives = self.test(agent)
            nb_pos = len(positives)

            if nb_pos == 0:
                a = np.argmax(self.emp_mean_h + self.deviation_h)
            else:
                B_t = np.zeros((2, nb_pos, K))
                B_t[0] = (
                    np.zeros((nb_pos, K))
                    + agent.emp_mean[positives]
                    + self.deviation
                )
                B_t[1] = (
                    np.zeros((nb_pos, K)) + self.emp_mean_h + self.deviation_h
                )
                B_t = np.min(B_t, axis=0)
                idx = np.argmax(np.max(B_t, axis=1))
                a = np.argmax(B_t[idx])
        return a

    def test(self, agent):
        Z = (
            np.abs(agent.emp_mean - self.emp_mean_h)
            - self.deviation
            - self.deviation_h
        )
        Z = np.max(Z, axis=1)

        positives = list(np.where(Z <= 0)[0])
        return positives

    def RTP(self, agent):
        m = self.M
        M2 = self.M2
        M3 = self.M3
        eigvals, eigvecs = eigh(M2)
        eigvals = eigvals[-m:]
        if min(eigvals) < 0:
            eigvals = eigvals + 1.2 * np.abs(min(eigvals))

        eigvecs = eigvecs[:, -m:]
        D = np.diag(1 / np.sqrt(eigvals))
        W = eigvecs.dot(D)
        B = pinv(W.T)

        T = np.einsum("uvw,ui,vj,wk->ijk", M3, W, W, W)

        L, N = 25, 15
        for l in range(m):
            temp_ = []
            temp = []
            for _ in range(L):
                theta = np.random.randn(m)
                theta = theta / norm(theta)
                theta = theta.reshape((m, 1))
                for _ in range(N):
                    theta_ = np.array(
                        [
                            [
                                theta.T.dot(T[i].dot(theta))[0, 0]
                                for i in range(m)
                            ]
                        ]
                    ).T
                    theta = theta_ / norm(theta_)
                temp.append(theta)
                value = T
                for _ in range(3):
                    value = np.tensordot(value, theta, axes=([0], [0]))
                temp_.append(value[0][0][0])
            tau = np.argmax(temp_)
            theta = temp[tau]

            for _ in range(N):
                theta_ = np.array(
                    [[theta.T.dot(T[i].dot(theta))[0, 0] for i in range(m)]]
                ).T
                theta = theta_ / norm(theta_)
            lambd = T
            for _ in range(3):
                lambd = np.tensordot(lambd, theta, axes=([0], [0]))
            lambd = lambd[0][0][0]
            temp = np.zeros((m, m, m))
            temp = temp + theta.dot(theta.T)
            temp = temp * theta.reshape((m, 1, 1))
            agent.emp_mean[l] = lambd * B.dot(theta.reshape(m)).reshape(self.K)
            T = T - lambd * temp

        delta = 1 / (agent.t + 1)
        C = 2
        self.deviation = C * np.sqrt(
            np.log((self.K * agent.H * 2 * m / delta)) / (agent.h + 1)
        )

    def update_moments(self, agent):
        h = agent.h
        R = agent.R
        muol = np.zeros((3, self.K))
        for a in range(self.K):
            samples = np.array(R[h][a])
            n_0 = int(len(samples) / 3)
            samples = samples[: 3 * n_0]
            samples = samples.reshape((n_0, 3))
            muol[:, a] = np.mean(samples, axis=0)

        mu_1 = muol[0].reshape((self.K, 1))
        mu_2 = muol[1].reshape((1, self.K))
        self.M2 = (1 / (h + 1)) * (h * self.M2 + mu_1.dot(mu_2))

        mu_1, mu_2 = mu_1.reshape(self.K), mu_2.reshape(self.K)
        mu_3 = muol[2]
        self.M3 = (1 / (h + 1)) * (
            h * self.M3 + np.einsum("j,k,l->jkl", mu_1, mu_2, mu_3)
        )
