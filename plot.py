import matplotlib.pyplot as plt
import numpy as np
import matplotlib.font_manager
import pylab
import matplotlib as mpl
from scipy import stats
import os

q = stats.norm.ppf(0.975)

mpl.rcParams["font.size"] = 22
mpl.rcParams["font.serif"] = "Computer Modern Roman"


def plot_regret(time, regret, it, fig_name, policies, H, T):
    _ = plt.figure(num=0)

    for i, pi in enumerate(policies):
        lab = pi.__str__()
        col = pi.__color__()

        regret_avg = np.mean(regret[i][: it + 1], axis=0)
        regret_std = np.std(regret[i][: it + 1], axis=0)

        plt.plot(time, regret_avg, label=lab, color=col)
        plt.fill_between(
            time,
            regret_avg - (q / np.sqrt(it + 1)) * regret_std,
            regret_avg + (q / np.sqrt(it + 1)) * regret_std,
            color=col,
            alpha=0.15,
        )
        plt.xlabel("Period h")
        plt.ylabel("Cumulative regret " + "$R(\\nu, h, T)$")
        if H % 10 == 0:
            plt.xticks(
                int(H / 10) * T * np.arange(1, 11),
                int(H / 10) * np.arange(1, 11),
            )
        elif H % 5 == 0:
            plt.xticks(
                int(H / 5) * T * np.arange(1, 6), int(H / 5) * np.arange(1, 6)
            )
        plt.xlim([1, H * T])
        plt.grid(color="k", linewidth=0.2)
        plt.legend()
        plt.rc("text", usetex=True)
        pylab.rc("font", family="serif", size=15)
        plt.tight_layout()
    plt.ioff()
    if os.path.isfile(fig_name):
        os.remove(fig_name)
    plt.savefig(fig_name, bbox_inches="tight", pad_inches=0.01)
    plt.cla()
    plt.clf()
